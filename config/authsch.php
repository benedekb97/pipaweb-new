<?php

return [
    'user' => [
        'class' => \App\Models\User::class,
        'fields' => [
            'name' => 'name',
            'email' => 'email',
            'username' => 'schacc',
            'authsch_id' => 'provider_user_id'
        ]
    ],

    'driver' => [
        'client_id' => env("AUTHSCH_CLIENT_ID"),
        'client_secret' => env("AUTHSCH_CLIENT_SECRET"),
        'redirect' => 'authsch.callback'
    ],

    'update_when_login' => false,

    'redirect_route' => 'home'
];