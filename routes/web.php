<?php

Route::get('', 'IndexController@index')->name('home');
Route::get('location/{location}', 'IndexController@location')->name('location');

Route::group(['prefix' => 'profile', 'as' => 'profile.', 'middleware' => 'auth'], function(){
    Route::get('', 'ProfileController@index')->name('index');
    Route::get('error', 'ProfileController@error')->name('error');

    Route::post('password/add/{user}', 'ProfileController@addPassword')->name('password.add');
    Route::post('password/change/{user}', 'ProfileController@changePassword')->name('password.change');
});

Route::post('location/{location}/change', 'IndexController@changeLocation')->name('location.change');

Route::post('login', 'IndexController@login')->name('login');

Route::get('logout', 'IndexController@logout')->name('logout');

Route::get('register', 'IndexController@register')->name('register');
Route::post('register', 'IndexController@saveRegistrar')->name('save_registrar');

Route::group(['prefix' => 'pipe', 'as' => 'pipe.', 'middleware' => 'pipe_admin'], function(){
    Route::post('modify/{pipe}/{location}', 'PipeController@modify')->name('modify');
    Route::post('add/{location}', 'PipeController@add')->name('add');
    Route::post('delete/{pipe}/{location}', 'PipeController@delete')->name('delete');
});

Route::group(['prefix' => 'admin/', 'as' => 'admin.', 'middleware' => 'admin'], function(){
    Route::get('', 'AdminController@index')->name('index');

    Route::get('regs', 'AdminController@regs')->name('regs');
    Route::post('regs/delete/{reg}', 'AdminController@deleteReg')->name('regs.delete');
    Route::post('regs/accept/{reg}', 'AdminController@acceptReg')->name('regs.accept');

    Route::get('pipes', 'AdminController@pipes')->name('pipes');

    Route::get('pipes/{pipe}', 'AdminController@showPipe')->name('pipe.show');
    Route::post('pipes/delete/{pipe}', 'AdminController@deletePipe')->name('pipe.delete');

    Route::group(['prefix' => 'locations', 'as' => 'locations.'], function(){
        Route::get('locations', 'AdminController@locations')->name('index');
        Route::get('{location}', 'AdminController@showLocation')->name('show');

        Route::post('delete/location/{location}', 'AdminController@deleteLocation')->name('delete');
        Route::post('add', 'AdminController@addLocation')->name('add');

        Route::post('delete/admin/{user}/location/{location}', 'AdminController@deleteAdmin')->name('delete_admin');
        Route::post('add/admin/location/{location}', 'AdminController@addAdmin')->name('add_admin');
    });

    Route::group(['prefix' => 'users', 'as' => 'users.'], function(){
        Route::get('', 'UsersController@index')->name('index');

        Route::post('delete/{user}', 'UsersController@delete')->name('delete');
        Route::post('admin/remove/{user}', 'UsersController@removeSuperAdmin')->name('remove_super_admin');
        Route::post('admin/add/{user}', 'UsersController@addSuperAdmin')->name('add_super_admin');
    });
});