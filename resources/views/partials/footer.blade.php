<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/validator.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#reg-form').validator();
    });
</script>

<div class="footer-text">
    Version 3<br>
    Written by: Benedek Burgess
</div>