<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Admin navigáció</h3>
        </div>
        <ul class="nav">
            <li>
                <a href="{{ route('admin.index') }}">Főoldal</a>
            </li>
            <li>
                <a href="{{ route('admin.locations.index') }}">Helyszínek</a>
            </li>
            <li>
                <a href="{{ route('admin.pipes') }}">Pipák</a>
            </li>
            <li>
                <a href="{{ route('admin.regs') }}">Regisztrációk</a>
            </li>
            <li>
                <a href="{{ route('admin.users.index') }}">Felhasználók</a>
            </li>
        </ul>
    </div>
</div>