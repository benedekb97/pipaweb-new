<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">Pipa.ml</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::guest())
                <li>
                    <a href="#" role="button" data-toggle="modal" data-target="#login-modal">Bejelentkezés</a>
                </li>
                @elseif(Auth::check())
                    @if(Auth::user()->super_admin)
                    <li>
                        <a href="{{ route('admin.index') }}">Admin</a>
                    </li>
                    @endif
                <li>
                    <a href="{{ route('profile.index') }}">Profilom</a>
                </li>
                <li>
                    <a href="{{ route('logout') }}">Kijelentkezés</a>
                </li>
                @endif

            </ul>
        </div>
    </div>
</nav>