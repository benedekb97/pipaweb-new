@extends('layouts.main')

@section('title', 'Felhasználónév igénylés')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Felhasználó igénylése</h3>
            </div>
            <div class="panel-body">
                <form action="{{ route('save_registrar') }}" method="POST" id="register-form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="input-group">
                            <label class="input-group-addon" for="name">Teljes név</label>
                            <input class="form-control" name="name" id="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label class="input-group-addon" for="email">Email cím</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <label class="input-group-addon" for="username">Kívánt felhasználónév</label>
                            <input class="form-control" id="username" name="username">
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer">
                <a href="#" onclick="document.getElementById('register-form').submit()" class="btn btn-default">Küldés</a>
            </div>
        </div>

        @if(isset($error))
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        @switch($error)
                            @case('email')
                                Email already in use
                            @break
                        @endswitch
                    </h3>
                </div>
            </div>
        @endif
    </div>
@endsection