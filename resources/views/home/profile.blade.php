@extends('layouts.main')

@section('title', 'Profil')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $user->name }}</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Felhasználónév</th>
                        <td>{{ $user->username }}</td>
                    </tr>
                    <tr>
                        <th>Név</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th>Csatlakozott</th>
                        <td>{{ $user->created_at->diffForHumans() }}</td>
                    </tr>
                    @if($user->locations->count() != 0)
                        <tr>
                            <th>Pipák száma</th>
                            <td>{{ $user->pipes->count() }}</td>
                        </tr>
                    @endif
                    <tr>
                        <th>AuthSCH</th>
                        <td>@if($user->authsch_id!=null) <i class="fa fa-check"></i> @else <i class="fa fa-times"></i> @endif</td>
                    </tr>
                    <tr>
                        <th>Jelszavas belépés</th>
                        <td>@if($user->password!=null) <i class="fa fa-check"></i> @else <i class="fa fa-times"></i> @endif</td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer">
                @if($user->password == null)
                    <button class="btn btn-default" data-toggle="modal" data-target="#add-password">Jelszó hozzáadása</button>
                @else
                    <button class="btn btn-default" data-toggle="modal" data-target="#change-password">Jelszó módosítása</button>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @if($user->password == null)
    <div class="modal fade" role="dialog" id="add-password">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Jelszó hozzáadása</h3>
                </div>
                <div class="modal-body">
                    <form id="add-password-form" action="{{ route('profile.password.add', [$user]) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon" for="password1">Jelszó</label>
                                <input type="password" class="form-control" id="password1" name="password1">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon" for="password2">Jelszó mégegyszer</label>
                                <input type="password" class="form-control" id="password2" name="password2">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">Mégse</button>
                    <button class="btn btn-primary" form="add-password-form">Mentés</button>
                </div>
            </div>
        </div>
    </div>
    @else
        <div class="modal fade" role="dialog" id="change-password">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Jelszó módosítása</h3>
                    </div>
                    <div class="modal-body">
                        <form id="add-password-form" action="{{ route('profile.password.change', [$user]) }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon" for="password_old">Régi jelszó</label>
                                    <input type="password" class="form-control" id="password_old" name="password_old">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon" for="password1">Jelszó</label>
                                    <input type="password" class="form-control" id="password1" name="password1">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon" for="password2">Jelszó mégegyszer</label>
                                    <input type="password" class="form-control" id="password2" name="password2">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal" type="button">Mégse</button>
                        <button class="btn btn-primary" form="add-password-form">Mentés</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts')
    @if(isset($error))
        <script>
            $('#add-password').modal().open();
            $('#change-password').modal().open();
        </script>
    @endif
@endsection