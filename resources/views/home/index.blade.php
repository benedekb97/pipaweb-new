@extends('layouts.main')

@section('title', 'Főoldal')

@section('content')
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading" style="text-align:center;">
                <a class="panel-title" data-toggle="collapse" href="#locations" style="text-align:center; width:100%; color:white;">
                    {{ $current_location->name }} - {{ $current_location->description }} &#x25BC;
                </a>
            </div>
            <div class="panel-collapse collapse" id="locations">
                <ul class="nav">
                    @foreach($locations as $location)
                        @if(!$location->hidden)
                            <li style="text-align:center;">
                                <a href="{{ route('location', [$location]) }}">{{ $location->name }} - {{ $location->description }}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @if($admin)
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Jelenlegi felszereltség</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Dohány</th>
                            <td>{{ $current_location->getTobaccoState() }} ({{ $current_location->tobacco_type }})</td>
                        </tr>
                        <tr>
                            <th>Szén</th>
                            <td>{{ $current_location->getCoalState() }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Jelenlegi pipa</h3>
                </div>
                @if($current_pipe != null)
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Dohány</th>
                            <td>{{ $current_pipe->type }}</td>
                        </tr>
                        <tr>
                            <th>Állapot</th>
                            <td>{{ $current_status }}</td>
                        </tr>
                    </table>
                </div>
                @else
                <div class="panel-body">
                    Jelenleg nincs pipa
                </div>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Pipadmin</h3>
                </div>
                <div class="panel-body">
                    @if($current_pipe != null)
                    <form id="pipe-form" action="{{ route('pipe.modify', [$current_pipe, $current_location]) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon" for="type">Dohány</label>
                                <input class="form-control" name="type" id="type" value="{{ $current_pipe->type }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon" for="status">Állapot</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="starting" @if($current_status == "Készül") selected @endif>Készül</option>
                                    <option value="good" @if($current_status == "Jó") selected @endif>Jó</option>
                                    <option value="dying" @if($current_status == "Kezd meghalni") selected @endif>Kezd meghalni</option>
                                    <option value="dead">Meghalt</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    @else
                        <form id="pipe-form" action="{{ route('pipe.add', [$current_location]) }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon" for="type">Dohány</label>
                                    <input class="form-control" name="type" id="type">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="input-group-addon" for="status">Állapot</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="starting">Készül</option>
                                        <option value="good">Jó</option>
                                        <option value="dying">Kezd meghalni</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
                <div class="panel-footer">
                    <input type="button" onclick="document.getElementById('pipe-form').submit()" value="Mentés" class="btn btn-primary">
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Dohadmin</h3>
                </div>
                <div class="panel-body">
                    <form id="change-location" action="{{ route('location.change', $current_location) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <label for="coal" class="input-group-addon">Szén</label>
                                <select class="form-control" id="coal" name="coal">
                                    <option value="0" @if($current_location->coal == 0) selected @endif>Nincs</option>
                                    <option value="1" @if($current_location->coal == 1) selected @endif>Fogyóban</option>
                                    <option value="2" @if($current_location->coal == 2) selected @endif>Van</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label for="tobacco" class="input-group-addon">Dohány</label>
                                <select class="form-control" id="tobacco" name="tobacco">
                                    <option value="0" @if($current_location->tobacco == 0) selected @endif>Nincs</option>
                                    <option value="1" @if($current_location->tobacco == 1) selected @endif>Fogyóban</option>
                                    <option value="2" @if($current_location->tobacco == 2) selected @endif>Van</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label for="tobacco-type" class="input-group-addon">Dohány típus</label>
                                <input id="tobacco-type" class="form-control" name="tobacco_type" value="{{ $current_location->tobacco_type }}">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-default" form="change-location">Mentés</button>
                </div>
            </div>
        </div>
    @else
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Jelenlegi felszereltség</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Dohány</th>
                            <td>{{ $current_location->getTobaccoState() }} ({{ $current_location->tobacco_type }})</td>
                        </tr>
                        <tr>
                            <th>Szén</th>
                            <td>{{ $current_location->getCoalState() }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Jelenlegi pipa</h3>
                </div>
                @if($current_pipe != null)
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Dohány</th>
                            <td>{{ $current_pipe->type }}</td>
                        </tr>
                        <tr>
                            <th>Állapot</th>
                            <td>{{ $current_status }}</td>
                        </tr>
                    </table>
                </div>
                @else
                <div class="panel-body">
                    Nincs jelenleg pipa
                </div>
                @endif
            </div>
        </div>
    @endif
@endsection