<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Not authorised!</title>
</head>
<body>
    <h1>Unauthorised request!</h1>
    <p>You are not authorised to view the page you requested!</p>
</body>
</html>