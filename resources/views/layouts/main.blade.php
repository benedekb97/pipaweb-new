<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Pipa.ml - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css') }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">
    @include('partials.nav')
    <div class="row">
        @yield('content')
    </div>
</div>
@include('partials.login_modal')
@include('partials.footer')
@yield('modals')
@yield('scripts')
</body>
</html>