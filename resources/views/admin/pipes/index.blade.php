@extends('layouts.admin')

@section('title', 'Pipák')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Pipák</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped" id="pipes-table">
                <thead>
                <tr>
                    <th>Típus</th>
                    <th>Létrehozta</th>
                    <th>Létrehozva</th>
                    <th>Állapot</th>
                    <th>Műveletek</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pipes as $pipe)
                    <tr>
                        <td>{{ $pipe->type }}</td>
                        <td>{{ $pipe->user->name }}</td>
                        <td>{{ $pipe->created_at }}</td>
                        <td>{{ $pipe->getStatusDiffForHumans() }}</td>
                        <td>
                            <a data-toggle="tooltip" title="Megtekint" class="btn btn-sm btn-default" href="{{ route('admin.pipe.show', $pipe) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        })
    </script>
@endsection