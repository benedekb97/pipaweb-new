@extends('layouts.admin')

@section('title', 'Pipa megtekintése')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Pipa megtekintése</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Dohány</th>
                    <td>{{ $pipe->type }}</td>
                </tr>
                <tr>
                    <th>Állapot</th>
                    <td>{{ $pipe->getStatusDiffForHumans() }}</td>
                </tr>
                <tr>
                    <th>Létrehozta</th>
                    <td>{{ $pipe->user->name }}</td>
                </tr>
                <tr>
                    <th>Létrehozva</th>
                    <td>{{ $pipe->created_at }}</td>
                </tr>
            </table>
        </div>
        <div class="panel-footer">
            <button class="btn btn-danger" data-toggle="modal" data-target="#delete-pipe">Törlés</button>
        </div>
    </div>
@endsection

@section('modals')
<div class="modal fade" role="dialog" id="delete-pipe">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Pipa törlése</h3>
            </div>
            <div class="modal-body">
                <h5>Biztosan kitörlöd ezt a pipát?</h5>
            </div>
            <div class="modal-footer">
                <form style="float:left;" action="{{ route('admin.pipe.delete', $pipe) }}" method="POST">
                    {{ csrf_field() }}
                    <input class="btn btn-danger" type="submit" value="Igen">
                    <button class="btn btn-success" type="button" data-dismiss="modal">Nem</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection