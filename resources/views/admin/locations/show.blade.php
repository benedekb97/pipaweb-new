@extends('layouts.admin')

@section('title', $location->name)

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Általános infók</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Név</th>
                    <td>{{ $location->name }}</td>
                </tr>
                <tr>
                    <th>Leírás</th>
                    <td>{{ $location->description }}</td>
                </tr>
                <tr>
                    <th>Pipák száma</th>
                    <td>{{ $location->pipes->count() }}</td>
                </tr>
                <tr>
                    <th>Adminok száma</th>
                    <td>{{ $location->admins->count() }}</td>
                </tr>
                <tr>
                    <th>Utolsó pipa</th>
                    <td>
                        @if($location->pipes->last())
                            {{ $location->pipes->last()->type }} |
                            {{ $location->pipes->last()->user->name }} |
                            {{ $location->pipes->last()->created_at }}
                        @else
                            <i>Még nem volt itt pipa</i>
                        @endif
                    </td>
                </tr>
            </table>
        </div>
        <div class="panel-footer">
            <button class="btn btn-default" data-toggle="modal" data-target="#add-admin">Admin hozzáadása</button>
            <button class="btn btn-danger" data-toggle="modal" data-target="#delete-location">Helyszín törlése</button>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Pipák</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped" id="pipes-table">
                <thead>
                <tr>
                    <th>Típus</th>
                    <th>Felhasználó</th>
                    <th>Állapot</th>
                    <th>Létrehozva</th>
                    <th>Műveletek</th>
                </tr>
                </thead>
                <tbody>
                @foreach($location->pipes as $pipe)
                    <tr>
                        <td>{{ $pipe->type }}</td>
                        <td>{{ $pipe->user->name }}</td>
                        <td>{{ $pipe->getStatusDiffForHumans() }}</td>
                        <td>{{ $pipe->created_at }}</td>
                        <td>
                            <a href="{{ route('admin.pipe.show', $pipe) }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Megtekint">
                                <i class="fa fa-eye"></i>
                            </a>
                            <span data-toggle="tooltip" title="Törlés">
                                <button class="btn btn-sm btn-danger" type="button" data-toggle="modal" data-target="#delete-pipe-{{ $pipe->id }}">
                                    <i class="fa fa-times"></i>
                                </button>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Típus</th>
                    <th>Felhasználó</th>
                    <th>Állapot</th>
                    <th>Létrehozva</th>
                    <th>Műveletek</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Adminok</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped" id="admins-table">
                <thead>
                <tr>
                    <th>Név</th>
                    <th>Műveletek</th>
                </tr>
                </thead>
                <tbody>
                @foreach($location->admins as $admin)
                    <tr>
                        <td>{{ $admin->name }}</td>
                        <td>
                            <span data-toggle="tooltip" title="Törlés">
                                <button class="btn btn-sm btn-danger" type="button" data-toggle="modal" data-target="#delete-admin-{{ $admin->id }}">
                                    <i class="fa fa-times"></i>
                                </button>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Név</th>
                    <th>Műveletek</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#pipes-table').dataTable();
            $('#admins-table').dataTable();
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection

@section('modals')
    @foreach($location->pipes as $pipe)
        <div class="modal fade" role="dialog" id="delete-pipe-{{ $pipe->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Pipa törlése</h3>
                    </div>
                    <div class="modal-body">
                        <h5>Biztosan kitörlöd a pipát?</h5>
                        <p>{{ $pipe->type }}</p>
                        <p>{{ $pipe->getStatusDiffForHumans() }}</p>
                    </div>
                    <form id="delete-form-{{ $pipe->id }}" action="{{ route('admin.pipe.delete', $pipe) }}" method="POST" style="display:none;">
                        {{ csrf_field() }}
                    </form>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" data-dismiss="modal">Nem</button>
                        <button class="btn btn-danger" form="delete-form-{{ $pipe->id }}">Igen</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @foreach($location->admins as $admin)
        <div class="modal fade" role="dialog" id="delete-admin-{{ $admin->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Admin törlése</h3>
                    </div>
                    <div class="modal-body">
                        <h3 class="modal-title">Biztosan eltávolítod ezt az adminisztrátort?</h3>
                        <p>{{ $admin->name }}</p>
                    </div>
                    <form style="display:none;" action="{{ route('admin.locations.delete_admin', [$admin, $location]) }}" method="POST" id="admin-form-{{ $admin->id }}">
                        {{ csrf_field() }}
                    </form>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">Nem</button>
                        <button class="btn btn-danger" form="admin-form-{{ $admin->id }}">Igen</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="modal fade" role="dialog" id="add-admin">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Admin hozzáadása</h3>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.locations.add_admin', $location) }}" method="POST" id="add-admin-form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon" for="user">Felhasználó</label>
                                <select name="user" id="user" class="form-control">
                                    @foreach($users as $user)
                                        @if($location->admins == null)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @elseif($location->admins->find($user) == null)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @elseif($location->admins->find($user)->first() == null)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">Mégse</button>
                    <button class="btn btn-primary" form="add-admin-form" type="submit">Hozzáadás</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" role="dialog" id="delete-location">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Helyszín törlése</h3>
                </div>
                <div class="modal-body">
                    <h5>Biztosan törlöd ezt a helyszínt?</h5>
                    @if($location->activePipe() != null)
                        <p class="warning">Jelenleg pipa van itt!!!</p>
                    @endif
                    <p>Név: {{ $location->name }}</p>
                    <p>Leírás: {{ $location->description }}</p>
                    <p>Pipák száma: {{ $location->pipes->count() }}</p>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('admin.locations.delete', $location) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger" value="Igen">
                        <button class="btn btn-success" type="button" data-dismiss="modal">Nem</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
