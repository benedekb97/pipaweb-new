@extends('layouts.admin')

@section('title', 'Helyszínek')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Helyszínek</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Név</th>
                    <th>Leírás</th>
                    <th>Pipák száma</th>
                    <th>Adminok száma</th>
                    <th>Műveletek</th>
                </tr>
                </thead>
                <tbody>
                @foreach($locations as $location)
                    <tr>
                        <td>{{ $location->name }}</td>
                        <td>{{ $location->description }}</td>
                        <td>{{ $location->pipes->count() }}</td>
                        <td>{{ $location->admins->count() }}</td>
                        <td>
                            <a data-toggle="tooltip" title="Megtekint" class="btn btn-sm btn-default" href="{{ route('admin.locations.show', $location) }}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <span data-toggle="tooltip" title="Törlés">
                                <button class="btn btn-sm btn-danger" type="button" data-toggle="modal" data-target="#delete-location-{{ $location->id }}">
                                    <i class="fa fa-times"></i>
                                </button>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <button class="btn btn-default" data-toggle="modal" data-target="#add-location-modal">Helyszín létrehozása</button>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" role="dialog" id="add-location-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Helyszín létrehozása</h3>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin.locations.add') }}" method="POST" id="add-location-form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon" for="name">Név</label>
                                <input id="name" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <label class="input-group-addon" for="description">Leírás</label>
                                <input id="description" name="description" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Mégse</button>
                    <button class="btn btn-primary" form="add-location-form">Mentés</button>
                </div>
            </div>
        </div>
    </div>

    @foreach($locations as $location)
        <div class="modal fade" role="dialog" id="delete-location-{{ $location->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Helyszín törlése</h3>
                    </div>
                    <div class="modal-body">
                        <h5>Biztosan törlöd ezt a helyszínt?</h5>
                        @if($location->activePipe() != null)
                            <p class="warning">Jelenleg pipa van itt!!!</p>
                        @endif
                        <p>Név: {{ $location->name }}</p>
                        <p>Leírás: {{ $location->description }}</p>
                        <p>Pipák száma: {{ $location->pipes->count() }}</p>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('admin.locations.delete', $location) }}" method="POST">
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger" value="Igen">
                            <button class="btn btn-success" type="button" data-dismiss="modal">Nem</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection