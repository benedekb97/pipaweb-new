@extends('layouts.admin')

@section('title', 'Regisztrációk')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Regisztrált felhasználók</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Név</th>
                    <th>Email</th>
                    <th>Kért felhasználónév</th>
                    <th>IP</th>
                    <th>Műveletek</th>
                </tr>
                </thead>
                <tbody>
                @foreach($regs as $reg)
                    <tr>
                        <td>{{ $reg->id }}</td>
                        <td>{{ $reg->name }}</td>
                        <td>{{ $reg->email }}</td>
                        <td>{{ $reg->username }}</td>
                        <td>{{ $reg->ip }}</td>
                        <td>
                            <span data-toggle="tooltip" data-placement="top" title="Törlés">
                                <button class="btn btn-danger" data-toggle="modal" data-target="#delete-reg-{{ $reg->id }}" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                            </span>
                            <span data-toggle="tooltip" data-placement="top" title="Elfogadás">
                                <button class="btn btn-success" data-toggle="modal" data-target="#accept-reg-{{ $reg->id }}" type="button">
                                    <i class="fa fa-check"></i>
                                </button>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection

@section('modals')
    @foreach($regs as $reg)
        <div class="modal fade" id="delete-reg-{{ $reg->id }}" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Regisztráció törlése</h3>
                    </div>
                    <div class="modal-body">
                        <h4>Biztosan törlöd a regisztrációt?</h4>
                        {{ $reg->name }}<br>
                        {{ $reg->email }}<br>
                        {{ $reg->username }}
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" data-dismiss="modal">Mégsem</button>
                        <form style="float:right; margin-left:10px;" action="{{ route('admin.regs.delete', [$reg]) }}" method="POST">
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger" value="Igen">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="accept-reg-{{ $reg->id }}" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Regisztráció elfogadása</h3>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('admin.regs.accept', [$reg]) }}" method="POST" id="register-{{ $reg->id }}">
                            @if($users->where('username', $reg->username)->first() != null)
                                <h5>Elfogadáshoz adj meg új felhasználónevet!</h5>
                                Név: {{ $reg->name }}<br>
                                Email: {{ $reg->email }}
                                <div class="form-group">
                                    <div class="input-group">
                                        <label class="input-group-addon" for="username">Új felhasználónév</label>
                                        <input id="username" name="username" class="form-control">
                                    </div>
                                </div>
                            @else
                                <h5>Biztosan elfogadod a regisztrációt?</h5>
                                Név: {{ $reg->name }}<br>
                                Email: {{ $reg->email }}<br>
                                Felhasználónév: {{ $reg->username }}
                                <input type="hidden" name="username" value="{{ $reg->username }}">
                            @endif
                            {{ csrf_field() }}
                            <input type="hidden" name="email" value="{{ $reg->email }}">
                            <input type="hidden" name="name" value="{{ $reg->name }}">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal">Mégse</button>
                        <input type="button" onclick="document.getElementById('register-{{ $reg->id }}').submit()" value="Igen" class="btn btn-success">
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection