@extends('layouts.admin')

@section('title', 'Felhasználók')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Felhasználók</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped" id="users-table">
                <thead>
                <tr>
                    <th>Felhasználónév</th>
                    <th>Név</th>
                    <th>Email cím</th>
                    <th>AuthSCH-s belépés</th>
                    <th>Jelszavas belépés</th>
                    <th>Csatlakozott</th>
                    <th>Műveletek</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>@if($user->authsch_id != null) <i class="fa fa-check"></i> @else <i class="fa fa-times"></i> @endif</td>
                        <td>@if($user->password != null) <i class="fa fa-check"></i> @else <i class="fa fa-times"></i> @endif</td>
                        <td>{{ $user->created_at->diffForHumans() }}</td>
                        <td>
                            @if($user->id != Auth::user()->id)
                            <span data-toggle="tooltip" title="Törlés">
                                <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-user-{{ $user->id }}">
                                    <i class="fa fa-times"></i>
                                </button>
                            </span>
                                @if($user->super_admin)
                                <form style="display:none;" method="POST" action="{{ route('admin.users.remove_super_admin', $user) }}" id="super-admin-{{ $user->id }}">{{ csrf_field() }}</form>
                                <span data-toggle="tooltip" title="Admin jogosultság megvonása">
                                    <button class="btn btn-sm btn-primary" form="super-admin-{{ $user->id }}">
                                        <i class="fa fa-user"></i>
                                    </button>
                                </span>
                                @else
                                <form style="display:none;" method="POST" action="{{ route('admin.users.add_super_admin', $user) }}" id="super-admin-{{ $user->id }}">{{ csrf_field() }}</form>
                                    <span data-toggle="tooltip" title="Admin jogosultság megadása">
                                    <button class="btn btn-sm btn-default" form="super-admin-{{ $user->id }}">
                                        <i class="fa fa-user-o"></i>
                                    </button>
                                </span>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Felhasználónév</th>
                    <th>Név</th>
                    <th>Email cím</th>
                    <th>AuthSCH-s belépés</th>
                    <th>Jelszavas belépés</th>
                    <th>Csatlakozott</th>
                    <th>Műveletek</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    @foreach($users as $user)
        <div class="modal fade" role="dialog" id="delete-user-{{ $user->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Felhasználó törlése</h3>
                    </div>
                    <div class="modal-body">
                        <h4>Biztosan törlöd ezt a felhasználót?</h4>
                        <p>Név: {{ $user->name }}</p>
                        <p>Felhasználónév: {{ $user->username }}</p>
                        <p>Email: {{ $user->email }}</p>
                        <p>Adminja: @foreach($user->locations as $location) {{ $location->name }}, @endforeach</p>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('admin.users.delete', $user) }}" method="POST">
                            {{ csrf_field() }}
                            <button class="btn btn-danger">Igen</button>
                            <button class="btn btn-default" data-dismiss="modal" type="button">Nem</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
@endsection

@section('scripts')
    <script>
        $('#users-table').dataTable();
        $('[data-toggle="tooltip"]').tooltip();
    </script>
@endsection