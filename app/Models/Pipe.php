<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pipe extends Model
{
    protected $fillable = [
        'created_float', 'type', 'location_id', 'created_by', 'created_at', 'modified_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function currentStatus()
    {
        $current_dead = time()-60*60;
        $current_dying = time()-45*60;
        $current_ready = time()-12*60;

        if($this->created_float < $current_dead){
            $current_status = 'dead';
        }elseif($this->created_float >= $current_dead && $this->created_float < $current_dying){
            $current_status = 'dying';
        }elseif($this->created_float >= $current_dying && $this->created_float < $current_ready){
            $current_status = 'good';
        }else{
            $current_status = 'starting';
        }

        return $current_status;
    }

    public function setStatus($new_status)
    {
        $times = [
            'dead' => time()-60*60,
            'dying' => time()-45*60,
            'ready' => time()-12*60,
            'starting' => time()
        ];

        $this->created_float = $times[$new_status];
    }

    public function getStatusDiffForHumans()
    {
        $statuses = [
            'good' => 'Jó',
            'starting' => 'Készül',
            'dying' => 'Kezd meghalni',
            'dead' => 'Meghalt'
        ];

        return $statuses[$this->currentStatus()];
    }
}
