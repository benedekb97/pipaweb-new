<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    static $states = [
        0 => 'Nincs',
        1 => 'Fogyóban',
        2 => 'Van'
    ];

    public function pipes()
    {
        return $this->hasMany(Pipe::class);
    }

    public function admins()
    {
        return $this->belongsToMany(User::class, 'user_location','location_id','user_id');
    }

    public function activePipe()
    {
        if($this->pipes->last() != null){
            if($this->pipes->last()->currentStatus() != "dead") {
                return $this->pipes->last();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getCoalState()
    {
        return self::$states[$this->coal];
    }

    public function getTobaccoState()
    {
        return self::$states[$this->tobacco];
    }
}
