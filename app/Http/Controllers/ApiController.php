<?php

namespace App\Http\Controllers;

use App\Models\Pipe;
use App\Models\Location;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function setPipeStatus(Request $request)
    {
        $location = Location::find(1);

        if($request->data == "no" && Location::find(1)->activePipe() != null) {
            $location->activePipe()->created_float = time()-60*60;
            $location->activePipe()->save();
        }

        if($request->data == "maybe") {
            if($location->activePipe() == null) {
                $pipe = new Pipe();
                $pipe->type = $location->tobacco_type;
                $pipe->location_id = 1;
                $pipe->created_by = 1;
                $pipe->created_float = time();
                $pipe->created_at = date("Y-m-d H:i:s");

                $pipe->save();
            } else {
                $location->activePipe()->created_float = time()-45*60;
                $location->activePipe()->save();
            }
        }

        if($request->data == "yes") {
            if($location->activePipe() == null) {
                $pipe = new Pipe();
                $pipe->type = $location->tobacco_type;
                $pipe->location_id = 1;
                $pipe->created_by = 1;
                $pipe->created_float = time()-12*60;
                $pipe->created_at = date("Y-m-d H:i:s");

                $pipe->save();
            } else {
                $location->activePipe()->created_float = time()-12*60;
                $location->activePipe()->save();
            }
        }

        return response()->json([
            'asd' => $request->data
        ]);
    }

    public function getPipeStatus()
    {

    }
}
