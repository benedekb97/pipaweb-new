<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Pipe;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PipeController extends Controller
{
    public function add(Location $location)
    {
        $statuses = [
            'starting' => time(),
            'good' => time()-12*60-1,
            'dying' => time()-45*60-1,
            'dead' => time()-60*60-1
        ];

        $pipe = new Pipe();

        $pipe->type = Input::get('type');
        $pipe->created_float = $statuses[Input::get('status')];
        $pipe->created_by = Auth::user()->id;
        $pipe->location_id = $location->id;

        $pipe->save();

        return redirect()->route('location', [$location]);
    }

    public function modify(Pipe $pipe)
    {
        $statuses = [
            'starting' => time(),
            'good' => time()-12*60-1,
            'dying' => time()-45*60-1,
            'dead' => time()-60*60-1
        ];

        $pipe->created_float = $statuses[Input::get('status')];
        $pipe->type = Input::get('type');

        $pipe->save();

        return redirect()->back();
    }
}
