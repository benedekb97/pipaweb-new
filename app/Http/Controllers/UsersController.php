<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();

        Carbon::setLocale('hu');

        return view('admin.users.index', ['users' => $users]);
    }

    public function delete(User $user)
    {
        if($user->id == Auth::user()->id){
            abort(418);
        }

        foreach($user->locations as $location){
            $location->admins()->detach($user->id);
        }
        foreach($user->pipes as $pipe){
            $pipe->created_by = 0;
        }
        $user->delete();

        return redirect()->route('admin.users.index');
    }

    public function removeSuperAdmin(User $user){
        $user->super_admin = 0;
        $user->save();

        return redirect()->back();
    }

    public function addSuperAdmin(User $user){
        $user->super_admin = 1;
        $user->save();

        return redirect()->back();
    }
}
