<?php

namespace App\Http\Controllers;

use App\Models\Registration;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Location;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    public function location($location)
    {
        $statuses = [
            'good' => 'Jó',
            'starting' => 'Készül',
            'dying' => 'Kezd meghalni'
        ];

        $location = Location::all()->where('id',$location)->first();
        $locations = Location::all();

        $admin = false;

        if(Auth::check()){
            if(Auth::user()->locations()->where('location_id', $location->id)->first() != null){
                $admin = true;
            }
        }

        if(!isset($location) || ($location->hidden && (!Auth::check() || !Auth::user()->super_admin))){
            abort(404);
        }

        $current_dead = time()-60*60;
        $current_dying = time()-45*60;
        $current_ready = time()-12*60;

        $current_pipe = $location->pipes()->where('created_float','>',time()-60*60)->first();

        if($current_pipe != null) {
            if($current_pipe->created_float >= $current_dead && $current_pipe->created_float < $current_dying){
                $current_status = $statuses['dying'];
            }elseif($current_pipe->created_float >= $current_dying && $current_pipe->created_float < $current_ready){
                $current_status = $statuses['good'];
            }else{
                $current_status = $statuses['starting'];
            }
        }else{
            $current_status = null;
        }

        return view('home.index', ['current_location' => $location, 'locations' => $locations, 'admin' => $admin, 'current_pipe' => $current_pipe, 'current_status' => $current_status]);
    }

    public function index()
    {
        $statuses = [
            'good' => 'Jó',
            'starting' => 'Készül',
            'dying' => 'Kezd meghalni'
        ];

        $location = Location::all()->find(1)->first();
        $locations = Location::all();

        $admin = false;

        $current_dead = time() - 60 * 60;
        $current_dying = time() - 45 * 60;
        $current_ready = time() - 12 * 60;

        $current_pipe = $location->pipes()->where('created_float', '>', time() - 60 * 60)->first();

        if($current_pipe != null) {
            if(($current_pipe->created_float >= $current_dead) && ($current_pipe->created_float < $current_dying)) {
                $current_status = $statuses['dying'];
            } elseif(($current_pipe->created_float >= $current_dying && $current_pipe->created_float < $current_ready)) {
                $current_status = $statuses['good'];
            } else {
                $current_status = $statuses['starting'];
            }
        }else{
            $current_status = null;
        }

        if(Auth::check()) {
            if(Auth::user()->locations->find($location->id) != null) {
                $admin = true;
            }
        }

        return view('home.index', [
            'current_location' => $location,
            'locations' => $locations,
            'admin' => $admin,
            'current_pipe' => $current_pipe,
            'current_status' => $current_status
        ]);
    }

    public function logout()
    {
        if(Auth::check()){
            Auth::logout();
        }

        return redirect()->route('home');
    }

    public function register()
    {
        return view('home.register');
    }

    public function saveRegistrar(Request $request)
    {
        $user = new Registration();

        $test = User::all()->where('email', Input::get('email'))->first();

        if($test != null){
            return view('home.register', ['error' => 'email']);
        }

        $user->name = Input::get('name');
        $user->email = Input::get('email');
        $user->username = Input::get('username');
        $user->ip = $request->ip();
        $user->save();

        return redirect()->route('home');
    }

    public function login()
    {
        if(Auth::validate([
            'username' => Input::get('username'),
            'password' => Input::get('password')
        ])){
            Auth::login(
                User::all()->where('username', Input::get('username'))->first()
            );

            return redirect()->route('home');
        } else {
            return redirect()->route('home');
        }
    }

    public function changeLocation(Location $location)
    {
        $location->coal = Input::get('coal');
        $location->tobacco = Input::get('tobacco');
        $location->tobacco_type = Input::get('tobacco_type');

        if(Input::get('tobacco_type') == "") {
            $location->tobacco_type = null;
        }

        $location->save();

        return redirect()->route('location', $location);
    }

    public function profile()
    {
        if(Auth::guest()){
            abort(401);
        }
        $user = Auth::user();
        Carbon::setLocale('hu');
        return view('home.profile', ['user' => $user]);
    }

    public function profileError()
    {
        if(Auth::guest()){
            abort(401);
        }
        $user = Auth::user();

        return view('home.profile', ['user' => $user, 'error' => 'password']);
    }

    public function addPassword(User $user)
    {
        if($user->password != null){
            abort(403);
        }
        if(strlen(Input::get('password1'))<6){
            return redirect()->route('profile.error');
        }elseif(Input::get('password1') != Input::get('password2')){
            return redirect()->route('profile.error');
        }else{
            $user->password = bcrypt(Input::get('password1'));
            $user->save();
        }

        return redirect()->route('profile');
    }

    public function changePassword(User $user)
    {
        if($user->password == null){
            abort(403);
        }

        if(Auth::validate([
            'username' => Auth::user()->username,
            'password' => Input::get('password_old')
        ])){
            if(strlen(Input::get('password1'))<6 || Input::get('password1') != Input::get('password2')){
                return redirect()->route('profile.error');
            }else{
                $user->password = bcrypt(Input::get('password1'));
                $user->save();
            }
        }else{
            return redirect()->route('profile.error');
        }
        return redirect()->route('profile');
    }
}
