<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    public function index()
    {
        if(Auth::guest()){
            abort(401);
        }
        $user = Auth::user();

        return view('home.profile', ['user' => $user]);
    }

    public function error()
    {
        if(Auth::guest()){
            abort(401);
        }
        $user = Auth::user();

        return view('home.profile', ['user' => $user, 'error' => 'password']);
    }

    public function addPassword(User $user)
    {
        if($user->password != null){
            abort(403);
        }
        if(strlen(Input::get('password1'))<6){
            return redirect()->route('profile.error');
        }elseif(Input::get('password1') != Input::get('password2')){
            return redirect()->route('profile.error');
        }else{
            $user->password = bcrypt(Input::get('password1'));
            $user->save();
        }

        return redirect()->route('profile.index');
    }

    public function changePassword(User $user)
    {
        if($user->password == null){
            abort(403);
        }

        if(Auth::validate([
            'username' => Auth::user()->username,
            'password' => Input::get('password_old')
        ])){
            if(strlen(Input::get('password1'))<6 || Input::get('password1') != Input::get('password2')){
                return redirect()->route('profile.error');
            }else{
                $user->password = bcrypt(Input::get('password1'));
                $user->save();
            }
        }else{
            return redirect()->route('profile.error');
        }
        return redirect()->route('profile.index');
    }
}
