<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Pipe;
use App\Models\Registration;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function regs()
    {
        $regs = Registration::all();
        $users = User::all();

        return view('admin.regs', ['regs' => $regs, 'users' => $users]);
    }

    public function deleteReg(Registration $reg)
    {
        $reg->delete();

        return redirect()->route('admin.regs');
    }

    public function acceptReg(Registration $reg)
    {
        $reg->delete();

        $user = new User();
        $user->name = Input::get('name');
        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->password = bcrypt(Input::get('username')."123");

        $user->save();

        return redirect()->route('admin.regs');
    }

    public function locations()
    {
        $locations = Location::all();

        return view('admin.locations.index', ['locations' => $locations]);
    }

    public function showLocation(Location $location)
    {
        $users = User::all();

        return view('admin.locations.show', ['location' => $location, 'users' => $users]);
    }

    public function showPipe(Pipe $pipe)
    {
        return view('admin.pipes.show', ['pipe' => $pipe]);
    }

    public function pipes()
    {
        $pipes = Pipe::all();

        return view('admin.pipes.index', ['pipes' => $pipes]);
    }

    public function deletePipe(Pipe $pipe)
    {
        $pipe->delete();

        return redirect()->route('admin.pipes');
    }

    public function deleteAdmin(User $user, Location $location)
    {
        $location->admins()->detach($user->id);

        return redirect()->back();
    }

    public function deleteLocation(Location $location)
    {
        foreach($location->pipes as $pipe){
            $pipe->delete();
        }

        foreach($location->admins as $admin){
            $location->admins()->detach($admin->id);
        }

        $location->delete();

        return redirect()->route('admin.locations.index');
    }

    public function addAdmin(Location $location)
    {
        $location->admins()->attach(Input::get('user'));

        return redirect()->back();
    }

    public function addLocation()
    {
        $location = new Location();

        $location->name = Input::get('name');
        $location->description = Input::get('description');

        $location->save();

        return redirect()->back();
    }
}
