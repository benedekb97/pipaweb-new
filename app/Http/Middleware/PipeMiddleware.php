<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class PipeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $current_location = explode('/', $request->path())[sizeof(explode('/', $request->path()))-1];

        if(!Auth::guard($guard)->check()) {
            abort(401);
        }elseif(!Auth::user()->locations()->where('location_id', $current_location)->first() != null){
            abort(403);
        }else{
            return $next($request);
        }

        return null;
    }
}
