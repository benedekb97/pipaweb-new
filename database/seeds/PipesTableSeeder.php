<?php

use App\Models\Pipe;
use Illuminate\Database\Seeder;

class PipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pipe::create([
            'type' => 'test preparing',
            'created_by' => 1,
            'location_id' => 1,
            'created_float' => time()-1
        ]);

        Pipe::create([
            'type' => 'test dead',
            'created_by' => 1,
            'location_id' => 1,
            'created_float' => time()-61*60
        ]);

        Pipe::create([
            'type' => 'test done',
            'created_by' => 1,
            'location_id' => 1,
            'created_float' => time()-13*60
        ]);

        Pipe::create([
            'type' => 'test dying',
            'created_by' => 1,
            'location_id' => 1,
            'created_float' => time()-46*60
        ]);
    }
}
