<?php

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::create([
            'name' => '520',
            'description' => 'A fehér gólyák klubszobája',
            'tobacco' => 0,
            'coal' => 0
        ]);
    }
}
